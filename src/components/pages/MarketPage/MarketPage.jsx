import './MarketPage.css'
import Header from '../Header/Header';
import React, { useEffect } from 'react';
import axios from 'axios';
import usePagination from '../../hooks/usePagination';
import { Link } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { setCoins } from '../../../app/states/market';

export default function CoinList () {
  const coins = useSelector((state) => state.market.coins)
  const dispatch = useDispatch()
  const {
    firstContentIndex,
    lastContentIndex,
    page,
    setPage,
    totalPages,
  } = usePagination({
    contentPerPage: 5,
    count: coins.length,
  });

  useEffect(() => {
    axios.get(`https://api.coingecko.com/api/v3/coins/`)
        .then(res => {
          const coins = res.data;
          dispatch(setCoins(coins))
        })
  }, [])


    return (
      <div>
        <div>
          <Header/>
        </div>
        <h1 className='head-text'>Последние обновления рынка</h1>
        <div className='header-coin'>
          <span>Монета</span>
          <span>Стоимость</span>
          <span>24-ч. изменение</span>
          <span>Рыночная капитализация</span>
        </div>
        <div className='list'>
        {coins.slice(firstContentIndex, lastContentIndex).map(coin => 
          <Link to={`/coin/${coin.id}`} key={coin.id} className='header-coin__name'>
            <img src={coin.image.small} alt="" />
            <div>{coin.name}</div>
            <div>{coin.market_data.current_price.usd.toFixed(2)} $</div>
            <div>{Math.round((coin.market_data.price_change_percentage_24h_in_currency.usd)*10)/100}%</div>
            <div>{coin.market_data.market_cap.usd} $</div>
          </Link>)}
          <div className="pagination">
              {[...Array(totalPages).keys()].map(pagination => (
                <button
                  onClick={() => setPage(pagination + 1)}
                  key={pagination}
                  className={`pagination__button ${page === pagination + 1 ? 'active' : ''}`}>
                  {pagination + 1}
                </button>
              ))}
          </div>
        </div>
      </div>
    )
}