import { useParams } from 'react-router-dom';
import Header from '../Header/Header'
import './CoinPage.css'
import { useEffect, useState } from 'react';
import axios from 'axios';

export default function CoinPage() {
  const { coinId } = useParams();
  const [coin, setCoin] = useState(null)

  useEffect(() => {
      axios.get(`https://api.coingecko.com/api/v3/coins/${coinId}`)
          .then(r => setCoin(r.data))
  }, [coinId])

  return (
    <div className='coin'>
        <div>
            <Header/>
        </div>
        <div className='coin__content'>
            <div className='coin__content_name'>
                <img src={coin?.image.large} alt="" />
                <span className='coin__content_name_text'>{coin?.name}</span>
            </div>
            <div className='coin__content_description'>
                <div className='coin__content_description_price'>
                    <span className='coin__content_description_price_text'>Rank: #{coin?.market_cap_rank}</span>
                    <span className='coin__content_description_price_text'>24h Change: {Math.round((coin?.market_data?.price_change_percentage_24h_in_currency?.usd)*10)/100} %</span>
                    <span className='coin__content_description_price_text'>Price: {coin?.market_data?.current_price?.usd.toFixed(2)}</span>
                </div>
                <div className='coin__content_description_content'>
                    <span>Bitcoin is the first successful internet money based on peer-to-peer technology; whereby no central bank or authority is involved in the transaction and production of the Bitcoin currency. It was created by an anonymous individual/group under the name, Satoshi Nakamoto. The source code is available publicly as an open source project, anybody can look at it and be part of the developmental process. Bitcoin is changing the way we see money as we speak. The idea was to produce a means of exchange, independent of any central authority, that could be transferred electronically in a secure, verifiable and immutable way. It is a decentralized peer-to-peer internet currency making mobile payment easy, very low transaction fees, protects your identity, and it works anywhere all the time with no central authority and banks. Bitcoin is designed to have only 21 million BTC ever created, thus making it a deflationary currency. Bitcoin uses the SHA-256 hashing algorithm with an average transaction confirmation time of 10 minutes. Miners today are mining Bitcoin using ASIC chip dedicated to only mining Bitcoin, and the hash rate has shot up to peta hashes. Being the first successful online cryptography currency, Bitcoin has inspired other alternative currencies such as Litecoin, Peercoin, Primecoin, and so on. The cryptocurrency then took off with the innovation of the turing-complete smart contract by Ethereum which led to the development of other amazing projects such as EOS, Tron, and even crypto-collectibles such as CryptoKitties.</span>
                </div>
            </div>
        </div>
    </div>
)
}