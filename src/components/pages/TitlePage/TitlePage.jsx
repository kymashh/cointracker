import Header from '../Header/Header'
import './TitlePage.css'
import React, { useEffect, useState } from 'react';
import axios from 'axios';


export default function Title() {
  const [crypto, setCrypto] = useState([])

  useEffect(() => {
    axios.get(`https://api.coingecko.com/api/v3/coins/markets?vs_currency=usd&order=market_cap_desc&per_page=4&page=1&sparkline=false`)
    .then(res => {
      const crypto = res.data;
      setCrypto(crypto)
    })
  }, [])

  return (
    <div>
       <Header />
       <main className='main'>
        <p className='main__title'>
          TRACK AND TRADE <br /> <span>CRYPTO CURRENCIES</span>
        </p>
       </main>
       <footer className='footer'>
       {crypto.map(crypto => 
          <div key={crypto.id} className='footer__main'>
            <img className='main__crypto_image' src={crypto.image} alt="" />
            <div>{crypto.name}</div>
            <div>{crypto.current_price} $</div>
          </div>)}
       </footer>
    </div>
  )
}