import { Link } from 'react-router-dom'
import './form.css'
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { setPassword, setUsername } from '../../../../app/states/auth';


export default function FORM({header, button, linkText, linkTo, value}) {
  // const [username, setUsername] = useState('');
  const username = useSelector((state) => state.auth.username)
  const password = useSelector((state) => state.auth.password)
  const dispatch = useDispatch ();
  // const [password, setPassword] = useState('');

  const handleUsernameChange = (event) => {
    dispatch(setUsername(event.target.value));
    };
    useEffect(() => {localStorage.setItem('username', username)}, [username])
    
    const handlePasswordChange = (event) => {
    dispatch(setPassword(event.target.value));
    };
    useEffect(() => {localStorage.setItem('password', password)}, [password])


  return (
    <form className='field' id='form'>
      <fieldset className='field__border'>
        <div className='field__border_a'>
          <Link to={linkTo} className='field__border_a_a' href="">{linkText}</Link>
        </div>
        <div className='field__border_login'>
          <h1 className='field__border_title'>{header}</h1>
          <legend>Логин</legend>
          <input className='field__border_input' 
          type="text" 
          id='name'
          value={username}
          onChange={handleUsernameChange}/>
          <legend>Пароль</legend>
          <input className='field__border_input' 
          type="password" 
          id='password'
          value={password}
          onChange={handlePasswordChange}/>
        </div>
        <div className='field__border_check'>
          <input type="checkbox" />
          <span>Я согласен на обработку своих данных</span>
        </div>
        <Link to={"/"} className='field__border_button'>
          {button}
        </Link>
      </fieldset>
    </form>
  )
}