import FORM from "./form/form";

export default function AuthPage({signUp, signIn}) {
  return (
    <div>
      {signIn && <FORM
        linkText = {'Зарегестрироваться'}
        linkTo = "/signUp"
        header = {'Вход'}
        button = {'Войти'}
      />}
      {signUp && <FORM
        linkText = {'Войти'}
        linkTo = "/signIn"
        header = {'Регистрация'}
        button = {'Зарегестрироваться'}
      />}
    </div>
  )
}