import { Link } from 'react-router-dom'
import './Header.css'

export default function Header() {
  return (
    <header className='header'>
      <span className='header__title'>COINTRACKER</span>
      <div className='header__main'>
        <Link to={"/"} className='header__main_a'>Главная страница</Link>
        <Link to={"/market"} className='header__main_a'>Маркет</Link>
        <Link to={"/information"} className='header__main_a'>Как это работает?</Link>
        <span>Наши партнеры</span>
      </div>
      <Link to={"/signIn"} className='header__button'>
        <span className='header__button-content'>Войти</span>
      </Link>
    </header>
  )
}