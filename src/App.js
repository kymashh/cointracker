import { BrowserRouter, Route, Routes } from 'react-router-dom';
import './App.css';
import HowItWorks from './components/pages/HowItWorks/HowItWorks';
import MarketPage from './components/pages/MarketPage/MarketPage';
import Title from './components/pages/TitlePage/TitlePage';
import AuthPage from './components/pages/AuthPage/AuthPage';
import CoinPage from './components/pages/CoinPage/CoinPage';
import { Provider } from 'react-redux'
import store from './app/store'



function App() {
  return (
    <Provider store={store}>
      <BrowserRouter>
      <Routes>
          <Route path="/" element={<Title/>} />
          <Route path="/signIn" element={<AuthPage signIn />} />
          <Route path="/signUp" element={<AuthPage signUp />} />
          <Route path="/market" element={<MarketPage/>} />
          <Route path="/coin/:coinId" element={<CoinPage/>} />
          <Route path="/information" element={<HowItWorks/>} />
        </Routes>
      </BrowserRouter>
    </Provider>
  );
}

export default App;
