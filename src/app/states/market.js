import { createSlice } from '@reduxjs/toolkit'

export const marketSlice = createSlice({
  name: 'market',
  initialState: {
    coins: [],
  },
  reducers: {
    setCoins: (state, action) => {
      state.coins = action.payload
    }
  },
})

export const { setCoins } = marketSlice.actions

export default marketSlice.reducer