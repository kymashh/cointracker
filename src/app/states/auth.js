import { createSlice } from '@reduxjs/toolkit'

export const authSlice = createSlice({
  name: 'auth',
  initialState: {
    value: []
  },
  reducers: {
    setUsername: (state, action) => {
      state.username = action.payload
    },
    setPassword: (state, action) => {
      state.password  = action.payload
    }
  },
})

export const { setUsername, setPassword } = authSlice.actions

export default authSlice.reducer