import { configureStore } from '@reduxjs/toolkit'
import { marketSlice } from './states/market'
import { authSlice } from './states/auth'



export default configureStore({
  reducer: {
    market: marketSlice.reducer,
    auth: authSlice.reducer
  },
})